#!/bin/sh

echo "

Planeta Informática - http://www.planeta.inf.br
"

echo "\e[34m
   ########  ####               ########     ###     ######  ##    ##
   ##     ##  ##                ##     ##   ## ##   ##    ## ##   ##
   ##     ##  ##                ##     ##  ##   ##  ##       ##  ##
   ########   ##     #######    ########  ##     ## ##       #####
   ##         ##                ##        ######### ##       ##  ##
   ##         ##                ##        ##     ## ##    ## ##   ##
   ##        ####               ##        ##     ##  ######  ##    ##

\e[0m
"
echo "***************************************************************************************"
echo "Instalador do Aplicativo para CCR Barcas V1.0.19".
echo "***************************************************************************************"

echo "

-----------------------------------------------------------------------------------------
Este pacote executará os passos necessários para o bom funcionamento do equipamento.
\e[31mCertifique-se de que o instalador esteja executando no ambiente correto.\e[0m
-----------------------------------------------------------------------------------------

"

echo " Começando em 3..."
sleep 1
echo "   Começando em 2..."
sleep 1
echo "  Começando em 1..."
sleep 1

sudo rm -f install_log.txt

echo "-----------LOG START-------------" > install_log.txt 2>&1
uname -a >> install_log.txt 2>&1
echo "------------SERIAL---------------" >> install_log.txt 2>&1
cat /serial >> install_log.txt 2>&1
echo  *** >> install_log.txt
echo "-------------DATE----------------" >> install_log.txt 2>&1
date >> install_log.txt 2>&1
echo "-------------DISK----------------" >> install_log.txt 2>&1
df -h >> install_log.txt 2>&1
echo "------------IFACES---------------" >> install_log.txt 2>&1
/sbin/ifconfig >> install_log.txt 2>&1
echo "-------------FREE----------------" >> install_log.txt 2>&1
free -h >> install_log.txt 2>&1
echo "-------------SWAP----------------" >> install_log.txt 2>&1
sudo /sbin/swapon >> install_log.txt 2>&1
echo "-------------CPU-----------------" >> install_log.txt 2>&1
cat /proc/cpuinfo >> install_log.txt 2>&1
echo "-------------ENV-----------------" >> install_log.txt 2>&1
cat /boot/armbianEnv.txt >> install_log.txt 2>&1
echo "------------LSUSB----------------" >> install_log.txt 2>&1
lsusb >> install_log.txt 2>&1
echo "------------LSMOD----------------" >> install_log.txt 2>&1
lsmod >> install_log.txt 2>&1
echo "----------PRE KILL---------------" >> install_log.txt 2>&1
#aconteceu em um VRD de não retornar do ps, então vamos rodar por 3 segundos
timeout -k 3 3 ps aux >> install_log.txt 2>&1
echo "---------------------------------" >> install_log.txt 2>&1

echo " -> \e[38;5;22mParando loops.\e[0m"

sudo kill `ps -aux | grep -v grep | grep loop | awk '{ print $2 }' | xargs` >> install_log.txt 2>&1

echo " -> \e[38;5;22mParando aplicação.\e[0m"

sudo killall daemon >> install_log.txt 2>&1
sudo killall node >> install_log.txt 2>&1
sudo killall vlib-server >> install_log.txt 2>&1
sudo pkill Jiga >> install_log.txt 2>&1

echo "----------POS KILL---------------" >> install_log.txt 2>&1
ps aux >> install_log.txt 2>&1


echo " -> \e[38;5;22mPreparando para deletar arquivos antigos.\e[0m"

if [ -d /home/pi/Validator ]; then
	sudo rm /home/pi/Validator -rf
fi

if [ -d /home/pi/binaries ]; then
	sudo rm /home/pi/binaries -rf
fi

sudo rm /home/pi/post_extract* >> install_log.txt 2>&1
sudo rm /home/pi/loop* >> install_log.txt 2>&1

echo "-------------LIST----------------" >> install_log.txt 2>&1
ls -la /home/pi >> install_log.txt 2>&1
echo "---------------------------------" >> install_log.txt 2>&1

echo " -> \e[38;5;22mPreparando para instalar os arquivos.\e[0m"

if [ -d /home/pi/ ]; then
	mv * /home/pi/
else
	mkdir /home/pi/
	mv * /home/pi/
fi

echo " -> \e[38;5;22mExtraindo arquivo de NPM.\e[0m"
cd /home/pi
tar -zxf npm-bin.tar.gz --directory /home/pi/ >> install_log.txt 2>&1
rm npm-bin.tar.gz

echo " -> \e[38;5;22mDevolve todos os arquivos ao usuário pi.\e[0m"
sudo chown pi.pi /home/pi -R

echo " -> \e[38;5;22mPreparando permissões da aplicação.\e[0m"

chmod +x /home/pi/binaries/*
chmod +x /home/pi/loop*

echo " -> \e[38;5;22mAtualizando IUC.\e[0m"

sudo /home/pi/binaries/sendfw IUC 10200129 /home/pi/binaries/IUC29.apdu >> install_log.txt 2>&1

echo " -> \e[38;5;22mAtualizando RCD.\e[0m"

sudo /home/pi/binaries/sendfw RC 000200BD /home/pi/binaries/R189.apdu >> install_log.txt 2>&1

echo " -> \e[38;5;22mCriando SWAP.\e[0m"

if [ ! -f /swap ]; then
	sudo dd if=/dev/zero of=/swap bs=1M count=512 >> install_log.txt 2>&1
	sudo chmod 0600 /swap >> install_log.txt 2>&1
	sudo mkswap /swap >> install_log.txt 2>&1
	sudo swapon /swap >> install_log.txt 2>&1
fi

LINE=`grep swapon /etc/rc.local | wc -l`
if [ "$LINE" -eq "0" ]; then
	sudo sed -i  '/^SER=.*/a swapon /swap' /etc/rc.local
fi

echo " -> \e[38;5;22mReduz o tempo de beep via uboot.\e[0m"

LINE=`grep "gpio clear 299" /boot/boot.cmd | wc -l`
if [ "$LINE" -eq "0" ]; then
	sudo sed -i  '/^setenv\ load_addr.*/a gpio clear 299' /boot/boot.cmd
fi

sudo mkimage -C none -A arm -T script -d /boot/boot.cmd /boot/boot.scr >> install_log.txt 2>&1

echo " -> \e[38;5;22mAguardando IUC...\e[0m"
while true; do
	if [ -c /dev/ttyIUC0 ]; then
		break;
	elif [ -c /dev/ttyIUC1 ]; then
		break;
	elif [ -c /dev/ttyIUC2 ]; then
		break;
	elif [ -c /dev/ttyIUC3 ]; then
		break;
	fi
	echo -n .
	sleep 1
done

echo " -> \e[38;5;22mAdiciona serial S1...\e[0m"

sudo rm -f /tmp/parte1.txt
grep -v 'overlays=' /boot/armbianEnv.txt > /tmp/parte1.txt
echo 'overlays=uart1 uart2 usbhost1 usbhost2 analog-codec' >> /tmp/parte1.txt
sudo cp /tmp/parte1.txt /boot/armbianEnv.txt
sudo mkimage -C none -A arm -T script -d /boot/boot.cmd /boot/boot.scr >> install_log.txt 2>&1


att_serv(){
    sed -i "s/ENDERECO_SERVIDOR_POST/$1/" loop_daemon.sh &&
    echo "Servidor escolhido -> $1" >> install_log.txt 2>&1
}

url=""

while true
    do
        SERVER1=Niteroi
        SERVER2=PracaXV
        SERVER3=TesteLab

        INFO1=6a9508103952d7cece0aea7e.getstatica.com:80
        INFO2=b80b66c7ec284eb6dd9ef694.getstatica.com:80
        INFO3=7ad9b0caa013d576da45136e.getstatica.com:80

        user=$( dialog --stdout --nocancel --menu 'Selecione o servidor:' 0 0 0   $SERVER1 $INFO1 $SERVER2 $INFO2 $SERVER3 $INFO3)

        if [ ${#user} > 0 ]; then
            clear >$(tty)
            dialog --title "Confirmação" --clear \
                --yesno "Confirmar para $user?" 0 0

                case $? in
                0)
                    clear >$(tty)
                    case $user in
                        $SERVER1)
                            url=$INFO1
                            break;;
                        $SERVER2)
                            url=$INFO2
                            break;;
                        $SERVER3)
                            url=$INFO3
                            break;;
                    esac
                esac
        fi
done

att_serv "$url"

echo " -> \e[38;5;22mCorrigindo data.\e[0m"

/home/pi/binaries/ntpfix-arm 10.88.1.6 10.88.1.3 >> install_log.txt 2>&1

echo "    +-------------------------------------------------------"
echo "    | Data atual: " `date`
echo "    | Servidor  : " $user $url
echo "    +-------------------------------------------------------"
echo "    Se a data estiver incorreta, reinicie e execute o comando ntpfix-arm conectado a Internet."

sync
sync

echo " -> \e[38;5;22mInstalação concluída. Reinicie a unidade com o comando 'sudo reboot'\e[0m"
