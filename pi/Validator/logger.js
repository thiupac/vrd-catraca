/* Copyright (C) Planeta Informática - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * 
 * @file loggers.js
 * @brief Require moment to display before all console info's and set a logger to files
 * @author Planeta Engineering <engenharia@planeta.inf.br>
 * @date 2019
 *
 */

var moment = require('moment');
function getTimestamp(){
    return moment().format("YYYY-MM-DD HH:mm:ss.SSS");
}

var logger = {
	debug: (...args) => {
        var header = `[${getTimestamp()}] debug:`;
		if (program.DEBUG) console.log(...args)
        console.log (header, ...args);
        

    },
    info:(...args) => {
        var header = `[${getTimestamp()}] info:`;
        console.log(header,...args);
    }
}

module.exports = logger;