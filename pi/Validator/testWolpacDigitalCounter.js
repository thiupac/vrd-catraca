"use strict";
/* global require, console, Buffer, process, setTimeout */
const wolpacDigitalCounter = require('./wolpacDigitalCounter');

const DISPLAY = 999999;

wolpacDigitalCounter.open('/dev/ttyS1');

wolpacDigitalCounter.getPort().on('error', function(err) {
    console.log('Error: ', err.message);
});

wolpacDigitalCounter.getPort().on('data', function(data) {
    switch (wolpacDigitalCounter.getCmdType(data)) {
        case wolpacDigitalCounter.CMD_RESET:
            console.info('Reset response, send display command after 2 seconds..');
            setTimeout(wolpacDigitalCounter.setEntry, 2000, DISPLAY);
            //txBuffer(prepareDisplayCounterCmd(DISPLAY));
            break;
        case wolpacDigitalCounter.CMD_SET_COUNTERS:
            console.info('Display response, send read counters');
            wolpacDigitalCounter.getEntry();
            break;
        case wolpacDigitalCounter.CMD_GET_COUNTERS:
            console.info('Reader counters response');
            let entry = "";
            let exit = "";
            for (let d = 3; d <= 8; d++) {
                entry += data[d];
            }
            for (let d = 9; d <= 14; d++) {
                exit += data[d];
            }
            console.info('Entry counter', entry, "Exit counter", exit);
            if (parseInt(entry) == DISPLAY) {
                console.info('Tests passed. Finish program...');
                process.exit(0);
            }
            break;
        default:
    }
});

wolpacDigitalCounter.reset();