/* Copyright (C) Planeta Informática - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * 
 * @file help.js
 * @brief Contains help function, activated with --help on console before run main.js
 * @author Planeta Engineering <engenharia@planeta.inf.br>
 * @date 2019
 *
 */

function printHelp (){
    console.log("Planeta Informática - VLIB Node sample 2019 \n")
	console.log("Command line options:")
	console.log("--help                                : Display this information")
	console.log("--display=<console, rcd, electron>    : Choose the display back-end, default: console")
	console.log("\tconsole                       : Simulate the screen on the console")
	console.log("\trcd                           : Use the RCD732 as display")
	console.log("\telectron                      : Use Electron browser as display")
	console.log("--sync                                : Use VLIB_OpenSync instead of VLIB_Open")
	console.log("--debug                               : Enable some extra debug communication information")
	console.log("--server=<host[:port]>                : Configure the PSP server address")
	console.log("--daemon=<path_to_daemon_binary>      : Set the path to Daemon if not running through network")
	console.log("--host=<host[:port]>                  : Set the Daemon address when not running locally")
	console.log("--lang=<en, pt, es>                   : Select display language, default: en")
    console.log("")
}

module.exports = {
    printHelp
}