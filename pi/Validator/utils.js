/* Copyright (C) Planeta Informática - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * 
 * @file utils.js
 * @brief File to transalte server status values and format date type
 * @author Planeta Engineering <engenharia@planeta.inf.br>
 * @date 2019
 *
 */
"use strict";
/* global require, module */

var moment = require('moment');

function translateServerStatus(status) {
    switch (status) {
        case 3:
            status = "BAD_DATA";
            break;
        case 2:
            status = "BAD_HTTP";
            break;
        case 1:
            status = "OK";
            break;
        case 0:
            status = "NOK";
            break;
        default:
            status = "UNK";
            break;
    }
    return status;
}

function translateUpdateStatus(update) {
    switch (update) {
        case 0:
            update = "INACTIVE";
            break;
        case 1:
            update = "OK";
            break;
        case 2:
            update = "DEADLOCK";
            break;
    }
    return update;
}

function getTimestamp(dateFormat) {
    return new moment().format(dateFormat);
}

module.exports = {
    translateServerStatus,
    translateUpdateStatus,
    getTimestamp
};