/* Copyright (C) Planeta Informática - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * 
 * @file commands.js
 * @brief File containing all main program instructions
 * @author Planeta Engineering <engenharia@planeta.inf.br>
 * @date 2019
 *
 */

const { program } = require("./main");
const client = require('dgram').createSocket('udp4');
const logger = require('./logger')

const commands = {
	getStatus: "get-status",
	disableClient: "disable-client",
	enableClient: "enable-client",
	cardProcessing: "card-processing",
	transaction: "transaction",
	cardRemoved: "card-removed",
	waitRemoval: "wait-removal",
	sendDisplay: "display"
}

enableClient = () => {
	var obj = { command: commands.enableClient }
	sendCmd(obj);
}

disableClient = () => {
	var obj = { command: commands.disableClient }
	sendCmd(obj);
}

getStatus = () => {
	var obj = { command: commands.getStatus }
	sendCmd(obj);
}

sendCmd = (obj) => {
	if (program.seq > 1000000) program.seq = 1;
	obj.message_id = program.seq++;
	program.lastcmd = obj;
	program.timeout = Date.now();

	var objStringify = JSON.stringify(obj);
	var message = new Buffer.from(objStringify);
	if (program.DEBUG) {
		logger.info(`Udp message ${obj.message_id} sent to host ${program.daemon_address} : ${program.daemon_port}`);
		logger.info(`Message: `, objStringify);
	}

	client.send(message, 0, message.length, program.daemon_port, program.daemon_address, (err, bytes) => {
		if (err) throw err;
	});
}

module.exports = {
	commands,
	enableClient,
	disableClient,
	getStatus,
	sendCmd,
	client
}
