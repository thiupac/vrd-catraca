/* Copyright (C) Planeta Informática - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * 
 * @file messages.js
 * @brief File containing all messages displayed on screen
 * @author Planeta Engineering <engenharia@planeta.inf.br>
 * @date 2019
 *
 */
"use strict";
/* global  module */

const en = {
    pleaseWait: "Please wait...",
    operationSuccess: "Operation Success",
    tapCard: "Tap your card",
    removeCard: "Remove your card",
    serverOff: "Server Offline",
    waitingConnection: "Waiting Connection",
    time: "Time",
    dateTimeFormat: "YYYY-MM-DD HH:mm:ss",
    dateFormat: "YYYY-MM-DD",
    closed: "CLOSED",
    pass: "PERMIT",
    carona: "TAKE A STEP BACK",
    instrucao: "And tap your card again"
};

const pt = {
    pleaseWait: "Aguarde...",
    operationSuccess: "Transação Aceita",
    operationFailed: "Transação Recusada",
    tapCard: "Aproxime o cartão",
    removeCard: "Remova o cartão",
    serverOff: "Servidor Offline",
    waitingConnection: "Aguardando conexão",
    time: "Tempo",
    dateTimeFormat: "DD/MM/YYYY HH:mm:ss",
    dateFormat: "DD/MM/YYYY",
    closed: "FECHADO",
    pass: "PASSE",
    carona: "DÊ UM PASSO PARA TRÁS",
    instrucao: "E REAPROXIME O CARTÃO",

    short: [
        "PASSE", //0
        "Erro de SW", //1
        "CARTÃO NEGADO", //2
        "APROXIME NOVAMENTE", //3
        "ERRO DE BANDEIRA", //4
        "APROXIME NOVAMENTE", //5
        "CARTÃO EXPIRADO", //6
        "APLICAÇÃO EXPIRADA", //7
        "APROXIME NOVAMENTE", //8
        "APLICAÇÃO NEGADA", //9
        "VERIFICAR CONFIG", //10
        "PROCURE SEU BANCO", //11
        "APROXIME NOVAMENTE", //12
        "FORA DE SERVIÇO", //13
        "FORA DE SERVIÇO", //14
        "FORA DE SERVIÇO", //15
        "AGUARDE", //16
        "LIMITE DE USO", //17
    ]
};

const es = {
    pleaseWait: "Espera...",
    operationSuccess: "Operación OK",
    tapCard: "Presente la tarjeta",
    removeCard: "Quitar la tarjeta",
    serverOff: "Servidor Offline",
    waitingConnection: "Esperando la conexión",
    time: "Tiempo",
    dateTimeFormat: "DD/MM/YYYY HH:mm:ss",
    dateFormat: "DD/MM/YYYY",
    closed: "CERRADO",
    pass: "PASAR",
    carona: "DA UN PASO ATRÁS",
    instrucao: "Y REAPROXIMAR LA TARJETA",

    short: [
    "--", //0
    "Error de SW", //1
    "Tarjeta denegada", //2
    "Error de lectura", //3
    "Error de marca", //4
    "Error de operación", //5
    "Tarjeta vencida", //6
    "Aplicación vencida", //7
    "Error desconocido", //8
    "Aplicación denegada", //9
    "Error de config", //10
    "Error de autent.", //11
    "Error indefinido", //12
    "Lector desactivado", //13
    "Lector desactivado", //14
    "Lector desactivado", //15
    "Límite de uso.", //16
    "Límite de uso!", //17
    ]

};

module.exports = {
    en,
    pt,
    es
};
