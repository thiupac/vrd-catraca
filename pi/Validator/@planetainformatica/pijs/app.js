'use-strict';
var util = require('./controllers/util.js');
var readers = require('./controllers/readers/readerController.js');
var debug = false;
exports.Debug = function enableDebug(deb)
{
    debug = deb;
}

exports.ListReaders = async function ListReaders() {
    return await readers.listReaders();
};

exports.SCardTransmit = async function SCardTransmit(reader, apdu) 
{
    if(debug)
        console.log(reader.id + " >> " + util.toHexString(apdu));

    let ret = await readers.scardTransmit(reader, apdu);

    if(debug)
        console.log(reader.id + " << " + util.toHexString(ret));

    return ret;
};

/*async function main()
{
    debug = true;
    var r = await readers.listReaders();
    for(let i = 0; i < r.length; i++)
    {
        let apdu = [0x00, 0xA4, 0x00, 0x00, 0x02, 0x03,0x00];
        r[i].debug = true;
        let ATR = await r[i].ATR;
        console.log(r[i].id  + " ATR: " + util.toHexString(ATR));
        let ret = await r[i].transmit(apdu);
    }
    process.exit();
}

main();*/
