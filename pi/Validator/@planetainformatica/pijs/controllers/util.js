exports.sleep = function sleep(ms) {
    return new Promise(resolve => {
        setTimeout(resolve, ms)
    });
}


exports.toHexString = function toHexString(byteArray) 
{
    if(byteArray === null || byteArray === undefined) return "NULL";
    return Array.from(byteArray, function (byte) {
        return ('0' + (byte & 0xFF).toString(16)).slice(-2);
    }).join('').toUpperCase();
}


exports.envelopeSamApdu = function envelopeSamApdu(slot, apdu)
{
    let envApdu = new Array();
    envApdu.push(0xFF & 0xFF);
    envApdu.push(0xFC & 0xFF);
    envApdu.push(slot & 0xFF);
    envApdu.push((apdu.length & 0x0100) >> 8);
    envApdu.push(apdu.length & 0xFF);
    for(let i = 0; i < apdu.length; i++)
        envApdu.push(apdu[i]);

    return envApdu;
}