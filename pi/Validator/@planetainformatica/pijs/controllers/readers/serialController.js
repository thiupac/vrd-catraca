'use strict';
var util = require('../util.js');
var SerialPort = require('serialport');
var sframe = require('./sframe.js');

const MAX_NUMBER_OF_SAMS = 4;

exports.listReaders = async function listReaders()
{
    this.debug = 1;
    //let readerIndex = 0;
    let portList = await SerialPort.list();
    let readers = new Array();
    for (let i = 0; i < portList.length; i++)
    {
        
        if (portList[i].vendorId != '21ab' && portList[i].vendorId != '21AB') continue;
        let serialport = new SerialPort(portList[i].comName, {
            baudRate: 115200
        });
        
        let apdu = [0xFE, 0xDF, 0x60, 0x00, 0x00]; // ATR
        await sframe.send(serialport, apdu);
        let rec = await sframe.receive(serialport);
        if (rec) {
            let obj = new Object();
            //obj.id = readerIndex++;
            obj.readerName = util.toHexString((rec || []).slice(0, (rec || []).length - 2)) + " " + portList[i].comName;
            obj.readerType = "SERIAL";
            obj.port = portList[i].comName;
            obj.readerProtocol = "SFRAME";
            obj.serialPort = serialport;            
            obj.needApduEnvelop = false;
            obj.apduEnvelopSlot = -1;            
            obj.transmit = async function(apdu)
            {
                let napdu = apdu;
                if(this.debug)
                    console.log(this.id + ' >> ' + util.toHexString(apdu));
   
                if(this.needApduEnvelop)
                    napdu = util.envelopeSamApdu(this.apduEnvelopSlot, apdu);
            
                await sframe.send(this.serialPort, napdu);
                let rec = await sframe.receive(this.serialPort);
            
                if(this.needApduEnvelop)
                    rec = rec.slice(0, rec.length - 2);
            
                if(this.debug)
                    console.log(this.id + ' << ' + util.toHexString(rec));

                return rec;
            };   
            //obj.debug = true;         
            //obj.ATR = await obj.transmit([0xFF, 0xCA, 0xE1, 0x00, 0x00]);
            readers.push(obj);

            let sams = []//await findSams(obj);
            
            let oobj = obj;

            for(let i = 0; i < sams.length; i++)
            {
                obj = new Object();
                //obj.id = readerIndex++;
                obj.readerName = util.toHexString(rec.slice(0, rec.length - 2)) + ":" + "SAM" +  sams[i].slot + " "  + portList[i].comName;
                obj.readerType = "SERIAL";
                obj.port = portList[i].comName;
                obj.readerProtocol = "SFRAME";
                obj.serialPort = serialport;
                obj.needApduEnvelop = true;
                obj.apduEnvelopSlot = sams[i].slot;                
                obj.debug = oobj.debug;
                obj.transmit = oobj.transmit;
                
                //obj.ATR = await obj.transmit([0xFF, 0xCA, 0xE1, 0x00, 0x00]);

                readers.push(obj);
            }
        }
        else
            serialport.close();

    }
    return readers;
};

async function findSams(reader)
{
    let sams = new Array();
    for(let i = 1; i < MAX_NUMBER_OF_SAMS; i++)
    {
        let apdu = [0xFF, 0xCA, 0xE1, 0x00, 0x00]; // ATR
        let napdu = util.envelopeSamApdu(i, apdu);       
        let rec = await reader.transmit(napdu);
        if(rec != null && rec.length >= 4 &&  rec[rec.length - 4] === 0x90 && rec[rec.length - 3] === 0x00)
        {
            let obj = new Object();
            obj.slot = i;
            sams.push(obj);
        }
    }

    return sams;
}
