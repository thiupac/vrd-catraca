'use strict';
var util = require('../util.js');
var cache = require('memory-cache');
var timeout = 2000;
var seq = 0;
const stx = 0x02;

exports.send = async function send(serialPort, apdu) {
    let tosend = encode(apdu);
    await serialPort.write(tosend);
};

exports.receive = async function receive(serialPort) {
    let d = new Date();
    let starttime = d.getTime();
    let data = new Array();
    while (d.getTime() - starttime < timeout)
    {
        let read = await serialPort.read();

        if (read !== null && read.length > 0)
        {
           

            for (let i = 0; i < read.length; i++) {
                data.push(read[i]);
            }

            let ret = await decode(data);
            if (ret.code === 1) // Chegou todo o pacote e o pacote � valido
            {
                serialPort.write([0x06]); // envia ack
                return ret.data;
            }
            else if (ret.code < 0) // Algum erro ocorreu, limpa o pacote
                read = new Array();

            // objcode === 0, ent�o espera mais dados
        }
        await util.sleep(1);
        d = new Date();
    }
    return null;
};

function encode(apdu) {
    let retapdu = new Array();
    if (apdu.length <= 255)
    {
        retapdu.push(stx & 0xFF);
        retapdu.push(seq & 0xFF);
        retapdu.push(apdu.length & 0xFF);
    }
    else
    {
        retapdu.push((stx | 0x80) & 0xFF);
        retapdu.push(seq & 0xFF);
        retapdu.push(((apdu.length & 0xFF00)>>8) & 0xFF);
        retapdu.push(apdu.length & 0xFF);
    }

    for (let i = 0; i < apdu.length; i++)
        retapdu.push(apdu[i]);

    let xor = genxor(retapdu);
    retapdu.push(xor);
    return retapdu;
};

async function decode(data)
{
    let obj = new Object();
    obj.code = -1;
    obj.data = new Array();
    let pktstart = -1;

    if (data.length === 0) {
        obj.code = 0;
        return obj;
    }

    for (let i = 0; i < data.length; i++)
    {
        if (data[i] === 0x02 || data[i] === 0x82) {
            pktstart = i;
            break;
        }
    }

    if (pktstart < 0) // ainda n�o chegou stx, pede pra descartar
    {
        obj.code = -1;
        return obj;
    }

    if ((data[pktstart] === 0x02 && data.length - pktstart < 3) || (data[pktstart] === 0x82 && data.length - pktstart < 4)) // menos de 3 bytes v�lidos, o que impossibilita ter recebido o sz do pacote
    { 
        obj.code = 0;
        return obj;
    }

    let pktsz = 0;
    if (data[pktstart] === 0x02)
        pktsz = data[pktstart + 2];
    else {
        pktsz = ((data[pktstart + 2] << 8) & 0xFF);
        pktsz |= data[pktstart + 3];
    }

    if ((data[pktstart] === 0x02 && data.length < pktsz + 4) || (data[pktstart] === 0x82 && data.length < pktsz + 5)) // faltam caracteres
    {
        obj.code = 0;
        return obj;
    }

    let datalen = 0;
    if (data[pktstart] === 0x02)
        datalen = pktsz + 4;
    else
        datalen = pktsz + 5;
    //chegou at� aqui, hora de verificar o xor
    for (let i = pktstart; i < datalen; i++)
    {
        obj.data.push(data[i]);
    }

    let xor = genxor(obj.data);
    if (xor === data[datalen - 1 + pktstart])
    {
        let tmp = obj.data;
        let sidx = 0;
        if (obj.data[0] === 0x02)
            sidx = 3;
        else
            sidx = 4;

        obj.data = new Array();
        for (let i = sidx; i < tmp.length; i++)
            obj.data.push(tmp[i]);
               
        obj.code = 1;
        return obj;
    }

    obj.code = -1;
    return obj;
};

function genxor(apdu) {
    let xor = 0;
    for (let i = 0; i < apdu.length; i++)
        xor ^= (apdu[i] & 0xff);
    return xor;
};
