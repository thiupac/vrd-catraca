﻿'use-strict';
var error = require('../error.js');
var util = require('../util.js');
var pcsclite = require('pcsclite');
var pcsc = pcsclite();
var readerDetected = false;
var readerConnected = false;
var haveResponse = false;
const MAX_NUMBER_OF_SAMS = 4;
var pcscreaders = new Array();

pcsc.on('reader', function(reader) 
{
    console.log('New reader detected', reader.name);
    pcscreaders.push(reader);
    readerDetected = true;
});

async function ATR(obj)
{
    let ATR = new Array();
    obj.device.on('status', function(status) 
    {
        if(status.state & this.SCARD_STATE_PRESENT)
        {
            obj.state = this.SCARD_STATE_PRESENT;
            if(status.atr)
            {
                ATR = status.atr;
            }
        }
        else if(status.state & this.SCARD_STATE_EMPTY)
        {
            obj.state = this.SCARD_STATE_EMPTY;
        }
    });

    if(obj.state !== 32) // 16 = SCARD_STATE_PRESENT
    {
        obj.device.connect({share_mode : pcsc.SCARD_SHARE_SHARED }, 
            function(err, protocol) 
            {
                if (err) 
                {
                    if(this.debug)
                        console.log(err);
                } 
                else 
                {   
                    obj.protocol = protocol;
                }
            });
    }

    for(let i = 0; i < 100; i++)
    {
        if(ATR.length > 4)
        {
            return ATR;
        }
        await util.sleep(10);
    }

    return ATR;
};

exports.listReaders = async function listReaders()
{
    for(let i = 0; i < 100; i++)
    {
        if(readerDetected)
            break;
        await util.sleep(10);
    }

    let readers = new Array();

    for (i = 0; i < pcscreaders.length; i++)
    {
        let obj = new Object();
        //obj.id = readerIndex++;
        obj.device = pcscreaders[i];
        obj.readerName = pcscreaders[i].name;
        obj.readerType = "PCSC";
        obj.protocol = 0;   
        obj.state = 0;  
        obj.ATR = await ATR(obj);
        obj.debug = false;
                
        let response = new Array();
        obj.transmit = async function(apdu)
        {
            let napdu = apdu;

            if(this.needApduEnvelop)
                napdu = util.envelopeSamApdu(this.apduEnvelopSlot, apdu);
        
            if(this.id === undefined)
                this.id = this.readerName;

            if(this.debug)
                console.log(this.id + ' >> ' + util.toHexString(apdu));
                   
            haveResponse = false;
            var bapdu = new Buffer(napdu.length);
            for(let i = 0; i < napdu.length; i++)
                bapdu[i] = napdu[i];

            obj.device.transmit(bapdu, 512, obj.protocol, function(err, data) 
            {
                if (err) 
                {
                    if(this.debug)
                        console.log(err);
                    
                    response = error.READER_NO_COM;
                    haveResponse = true;

                } 
                else 
                {
                    response = data;
                    haveResponse = true;
                }
            });
            
            for(let i = 0; i < 5000; i++)
            {
                if(haveResponse)
                {
                    if(this.needApduEnvelop)
                        response = response.slice(0, response.length - 2);
                    if(this.debug)
                        console.log(this.id + ' << ' + util.toHexString(response));
                    return response;
                }
                await util.sleep(1);
            }

            if(this.debug)
                console.log(this.id + ' << ' + util.toHexString(error.READER_NO_COM));
        
            return error.READER_NO_COM;
        };

        if(obj.ATR.length <= 4) continue;
        readers.push(obj);

        if(obj.readerName.includes("Planeta"))
        {
            let sams = await findSams(obj);

            let oobj = obj;

            for(let i = 0; i < sams.length; i++)
            {
                let obj = new Object();
                //obj.id = readerIndex++;
                obj.device = oobj.device;
                obj.readerName = oobj.name + "SAM" + sams[i].slot;
                obj.readerType = "PCSC";
                obj.protocol = oobj.protocol;   
                obj.state = oobj.state;   
                obj.needApduEnvelop = true;
                obj.apduEnvelopSlot = sams[i].slot; 
                obj.transmit = oobj.transmit;
                obj.ATR = await obj.transmit([0xFF, 0xCA, 0xE1, 0x00, 0x00]);
                readers.push(obj);
            }
        }
    }

    return readers;
};

async function findSams(device)
{
    let sams = new Array();
    for(let i = 1; i < MAX_NUMBER_OF_SAMS; i++)
    {
        let apdu = [0xFF, 0xCA, 0xE1, 0x00, 0x00]; // ATR
        let napdu = util.envelopeSamApdu(i, apdu);
        let rec = await device.transmit(napdu);
        if(rec != null && rec.length >= 4 &&  rec[rec.length - 4] === 0x90 && rec[rec.length - 3] === 0x00)
        {
            let obj = new Object();
            obj.slot = i;
            sams.push(obj);
        }
    }

    return sams;
}