﻿'use-strict';
var cache = require('memory-cache');
var serial = require('./serialController.js');
//var pcsc = require('./pcscController.js');

exports.listReaders = async function listReaders()
{
    let readers = new Array();
    //let pcscReaders = await pcsc.listReaders();
    let serialReaders = await serial.listReaders();
    let readerIndex = 0;

    // for (let i = 0; i < pcscReaders.length; i++) {
    //     let obj = pcscReaders[i];
    //     obj.id = readerIndex++;
    //     readers.push(obj);
    // }

    for (let i = 0; i < serialReaders.length; i++) {
        let obj = serialReaders[i];
        obj.id = readerIndex++;
        readers.push(obj);
    }
    
    //cache.put("PSCS_READER_LIST", pcscReaders);
    cache.put("SERIAL_READER_LIST", serialReaders);
    cache.put("READER_LIST", readers);
    return readers;
};
