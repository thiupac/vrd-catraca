/* Copyright (C) Planeta Informática - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * 
 * @file display_console.js
 * @brief Contains all commands to display on console
 * @author Planeta Engineering <engenharia@planeta.inf.br>
 * @date 2019
 *
 */

var lines;
var lastStatus = {
    vlib_version: "",
    sam_version: "",
    reader_version: "",
    server_status: "",
    update_status: "",
    avr_pending: ""
};

function sendDisplay(lineNumber, textMessage) {
    lines[lineNumber] = centerText(textMessage);
    printDisplay();
}

function printDisplay() {
    console.clear();
    console.log("+----------------------+        +----------------------------------+")
    console.log("|" + lines[1] + "|             Vlib Version: " + lastStatus.vlib_version);
    console.log("|" + lines[2] + "|             Sam Version: " + lastStatus.sam_version.toString(16));
    console.log("|" + lines[3] + "|             Reader Version: " + lastStatus.reader_version.toString(16));
    console.log("|" + lines[4] + "|             Server Status: " + lastStatus.server_status);
    console.log("+----------------------+             Update Status: " + lastStatus.update_status);
    console.log("                                     AVR Pending:  " + lastStatus.avr_pending)
    console.log("                                +----------------------------------+")
}
async function initDisplay() {
    resetDisplay();
}

function resetDisplay() {
    lines = [false, centerText(""), centerText(""), centerText(""), centerText("")]
}

function sendStatus(status) {
    lastStatus = status;
}

module.exports = {
    sendDisplay,
    resetDisplay,
    sendStatus,
    initDisplay
}

function centerText(text) {
    const lineSize = 22;
    text = text || "";
    let length = text.length;
    if (length < lineSize) {
        let diff = lineSize - length;
        let padLength = Math.floor(diff / 2);
        text = text.padStart(padLength + text.length);
        text = text.padEnd(lineSize);
    }
    return text;
}