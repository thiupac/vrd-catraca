/* Copyright (C) Planeta Informática - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * 
 * @file main.js
 * @brief ValidatorSample main program
 * @author Planeta Engineering <engenharia@planeta.inf.br>
 * @date 2019
 *
 */
"use strict";
/* global require, setTimeout, process */
var ip = require('ip');


const { nin1_catracaGirou, nin0_barcaCheia, pout0_liberaCatraca, pout1_pictograma, 
    backlight, buzzer, beepT, rx232_checaCarona, tx232_confirmaGiro } = require('./rcd_hw');
var program = {
    display: 'rcd'
}
const { initDisplay, sendDisplay, resetDisplay, sendStatus, setLeds } = require("./display_" + program.display);

function sendMsgs(msg1, msg2, msg3) {
    sendDisplay(1, msg1 == undefined ? "" : msg1);
    sendDisplay(2, msg2 == undefined ? "" : msg2);
    sendDisplay(3, msg3 == undefined ? "" : msg3);
}

function troggleLeds() {
    pout0_liberaCatraca.writeSync(0);
    pout1_pictograma.writeSync(0);
    tx232_confirmaGiro.writeSync(1);

    setTimeout(() => {
        //backlight.writeSync(0);
        pout0_liberaCatraca.writeSync(1);
        pout1_pictograma.writeSync(1);
        tx232_confirmaGiro.writeSync(0);
    }, 500);
}

function testIOs() {
    sendDisplay(2, "Aguardando giro...");
    nin1_catracaGirou.watch((err, val) => {
        //console.log("val", val);
        if (!val) {
            sendDisplay(2, "Catraca girou!");
            nin1_catracaGirou.unwatchAll();
            sendDisplay(2, "Aguardando carona presente...");
            rx232_checaCarona.watch((err, val) => {
                if (!val) {
                    sendDisplay(2, "Carona presente!");
                    rx232_checaCarona.unwatchAll();
                    sendDisplay(2, "Aguardando desabilita VRD...");
                    let disabled = false;
                    nin0_barcaCheia.watch((err, val) => {
                        //console.log("val", val);
                        if (!val) {
                            if (!disabled) {
                                sendMsgs();
                                sendDisplay(1, "VRD desabilitado!");
                                backlight.writeSync(1);
                                disabled = true;
                            }
                        } else {
                            sendDisplay(1, "VRD habilitado!");
                            backlight.writeSync(0);
                            nin0_barcaCheia.unwatchAll();
                            sendMsgs();
                            sendDisplay(2, "Fim!!!");
                            setTimeout(() => {
                                sendMsgs();
                                setTimeout(() => {
                                    process.exit(0);
                                }, 2000);
                            }, 5000);
                        }
                    });
                }
            });
        }
    });
}

async function main() {
    tx232_confirmaGiro.writeSync(1);
    await initDisplay();
    sendDisplay(4, ip.address());
    beepT(500);

    nin0_barcaCheia.watch((err, val) => {
        if (!val) {
            sendDisplay(1, "VRD desabilitado!");
            backlight.writeSync(1);
        } else {
            sendDisplay(1, "VRD habilitado!");
            backlight.writeSync(0);
        }
        nin0_barcaCheia.unwatchAll();
        troggleLeds();
        testIOs();
    });
}

main();