/* Copyright (C) Planeta Informática - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * 
 * @file main.js
 * @brief ValidatorSample main program
 * @author Planeta Engineering <engenharia@planeta.inf.br>
 * @date 2019
 *
 */
"use strict";
/* global require, process, setInterval, setTimeout, clearTimeout, clearInterval, module */
const { spawn } = require('child_process');
const args = require('minimist')(process.argv);
const { printHelp } = require("./help");
const GPIOs = require('./rcd_hw');

const fs = require('fs');
const ini = require('ini');
const config = ini.parse(fs.readFileSync('./config.ini', 'utf-8'));
const timers = config.timers;

const OPER_CLOSED = 0;
const OPER_OPEN = 1;
const OPER_PASS = 2;
const OPER_STOP = 4;

const ip = require('ip');
const wolpacDigitalCounter = require('./wolpacDigitalCounter');
const wolpacDevice = '/dev/ttyS1';
const wolpacLock = '/tmp/wolpac.lock';
let entries = 0;
let fromClosed = false;

let pictogramaNoPass = false;
let ipTime;

GPIOs.beepT(500);
GPIOs.backlight.writeSync(1);

var program = {
    seq: 1,
    stage: 0,
    timeout: 0,
    lastcmd: 0,
    timestamp: 0,
    display: args.display,
    DEBUG: args.debug,
    daemon: args.daemon,
    sync: args.sync,
    language: args.lang,
    psp_server: args.server,
    operationalStatus: OPER_CLOSED
};

const logger = require('./logger');
const messageData = require('./messages');
const { translateServerStatus, translateUpdateStatus, getTimestamp } = require('./utils');

if (args.help) {
    printHelp();
    process.exit(0);
}

if (!program.language || !messageData[program.language]) {
    program.language = "pt";
}

const messages = messageData[program.language];

if (!program.display) {
    program.display = "rcd";
}
if (!(program.display == "rcd" || program.display == "console" || program.display == "electron")) {
    logger.info("Invalid display back-end");
    process.exit(1);
}
logger.info("Using display back-end: ", program.display);
const { initDisplay, sendDisplay, resetDisplay, sendStatus, setLeds } = require("./display_" + program.display);

logger.info("Using Wolpac digital counter:", wolpacDevice);
wolpacDigitalCounter.open(wolpacDevice);

wolpacDigitalCounter.getPort().on('error', function(err) {
    logger.log('Error: ', err.message);
});

wolpacDigitalCounter.getPort().on('data', function(data) {
    switch (wolpacDigitalCounter.getCmdType(data)) {
        case wolpacDigitalCounter.CMD_RESET:
            logger.info('Reset response');
            entries = 0;
            break;
        case wolpacDigitalCounter.CMD_SET_COUNTERS:
            logger.info('Display response');
            break;
        case wolpacDigitalCounter.CMD_GET_COUNTERS:
            logger.info('Reader counters response');
            let entry = "";
            let exit = "";
            for (let d = 3; d <= 8; d++) {
                entry += data[d];
            }
            for (let d = 9; d <= 14; d++) {
                exit += data[d];
            }
            logger.info('Entry counter', entry, "Exit counter", exit);
            entries = parseInt(entry);
            break;
        default:
            logger.info('Unexpected Wolpac cmd', wolpacDigitalCounter.getCmdType(data));
    }
});

async function main() {
    await initDisplay();
    if (program.daemon) {
        var daemonArgs = [];
        logger.info("Working via Daemon");
        program.daemon_port = 3001;
        if (program.psp_server != undefined) {
            daemonArgs.push(program.psp_server);
        }
        if (program.sync != undefined) {
            daemonArgs.push("-s");
        }
        let child = spawn(program.daemon, daemonArgs);
    } else if (program.psp_server) {
        logger.info("Process Exit.");
        process.exit();
    } else {
        logger.info('Working via Network');
        if (typeof args.host == "string") {
            var daemon_host = args.host.split(":");
            if (daemon_host.length == 2) {
                program.daemon_address = daemon_host[0];
                program.daemon_port = daemon_host[1];
            } else {
                logger.info('Using default server and port');
                program.daemon_address = daemon_host;
                program.daemon_port = 3001;
            }
        } else {
            program.daemon_address = "127.0.0.1";
            program.daemon_port = 3001;
        }
        if (!program.daemon_port || program.daemon_port < 1 || program.daemon_port > 65535) {
            logger.debug("Missing valid parameter port: " + program.daemon_port);
            process.exit(1);
        }
        if (program.daemon_address == "") {
            logger.debug("Missing valid parameter daemon_host");
            process.exit(1);
        }
        logger.info("IP:" + program.daemon_address + ", PORT:" + program.daemon_port);

    }
    try {
        if (fs.existsSync(wolpacLock)) {
            // main process has crashed so need to get current counts
            logger.info("Get counters...");
            wolpacDigitalCounter.getEntry();
        } else {
            logger.info("Reset counters...");
            wolpacDigitalCounter.reset();
            fs.openSync(wolpacLock, 'a');
        }
    } catch (err) {
        logger.error(err);
    }
    ipTime = new Date();
    checkNextStatus();
    setInterval(loop, 1000);

}

function loop() {
    if (program.operationalStatus == OPER_OPEN) {
        if (fromClosed) {
            fromClosed = false;
            wolpacDigitalCounter.reset();
        }
        setLeds(0, 0);
        if (program.stage == 0) {
            enableClient();
            setTimeout(() => { if (program.stage == 1) sendDisplay(1, messages.waitingConnection); }, timers.operOpen);
            program.stage = 1;
        }

        if (program.stage == 2) {
            program.stage = 3;
            getStatus();
        }

        if (Date.now() - program.timeout > 5000 && (program.stage == 1 || program.stage == 3)) {
            program.stage = 0;
            if (program.DEBUG) {
                logger.info("Timeout.");
            }
        }
        checkNextStatus();
    }
    if (program.operationalStatus == OPER_CLOSED) {
        disableClient();
        program.stage = 0;
        setLeds(0, 1);
        sendDisplay(1, messages.closed);
        sendDisplay(2, "");
        sendDisplay(3, "");

        checkNextStatus();
    }
    if (program.operationalStatus == OPER_PASS) {
        disableClient();
        setLeds(1, 0);
        sendDisplay(2, messages.pass);

    }
    if (program.operationalStatus == OPER_STOP) {
        disableClient();
        /*
            As in OPER_CLOSED, need to set stage to 0 since
            when returning to OPER_PASS there was a inconsistency and 
            took more time to show the correct message
        */
        program.stage = 0;
        setLeds(0, 1);
        sendDisplay(1, "");
        sendDisplay(2, messages.carona);
        sendDisplay(3, messages.instrucao);

        checkNextStatus();

    }

    var now = new Date();

    if (timeDiff(now.getTime(), ipTime.getTime()) > 15) {
        /*
            The date and time is presented when in operation
            and just the date when closed
        */
        if (program.operationalStatus == OPER_CLOSED) {
            sendDisplay(4, getTimestamp(messages.dateFormat));
        } else {
            sendDisplay(4, getTimestamp(messages.dateTimeFormat));
        }
    } else {
        sendDisplay(4, ip.address());
    }
}

function timeDiff(atual, ant) {
    var diff = atual - ant;
    return Math.round(diff / 1000);
}


function parseProtocol(message, remote) {
    var obj = JSON.parse(message);
    if (program.DEBUG) {
        logger.info("Response: \n", obj);

    }
    program.timeout = Date.now();
    if (obj.command == commands.cardProcessing) {
        sendDisplay(1, messages.pleaseWait);
        sendDisplay(2, "");
        sendDisplay(3, "");
        clearTimeout(program.timer);

    } else if (obj.command == commands.transaction) {

        if (obj.status == "OK") {
            sendDisplay(1, messages.operationSuccess);
            allowPass();
        } else {
            setLeds(0, 1);
            sendDisplay(1, messages.operationFailed);
            if (messages.short[obj.short_status] !== undefined) {
                sendDisplay(2, messages.short[obj.short_status]);
            } else {
                sendDisplay(2, obj.msg);
            }
            setTimeout(() => {
                if (program.operationalStatus == OPER_OPEN) {

                    sendDisplay(1, messages.tapCard);
                    sendDisplay(2, "");
                }
            }, timers.statusTap);
        }
    } else if (obj.command == commands.cardRemoved) {
        program.timer = setTimeout(function() {
            sendDisplay(3, "");
        }, timers.commRemoved);

    } else if (obj.command == commands.waitRemoval) {
        sendDisplay(3, messages.removeCard);
    } else if (obj.command == commands.sendDisplay) {
        sendDisplay(obj.line + 1, obj.text ? obj.text : "");
    } else {
        if (program.lastcmd.command == commands.enableClient) {
            if (program.DEBUG) {
                logger.info(`Message received ${obj.message_id}: ${obj.status} `);
            }
            if (obj.status == "OK") {
                sendDisplay(1, messages.tapCard);
                sendDisplay(2, "");
                sendDisplay(3, "");
                program.stage = 2;
            }
        } else if (program.lastcmd.command == commands.getStatus) {
            obj.server_status = translateServerStatus(obj.server_status);
            obj.update_status = translateUpdateStatus(obj.update_status);

            if (obj.server_status != "OK") {
                sendDisplay(3, messages.serverOff);
            } else {
                sendDisplay(3, "");
            }
            if (obj.registered == 0)
                program.stage = 0;
            else {
                sendStatus(obj);
                program.stage = 2;
            }
        }
    }
}

function allowPass() {
    disableClient();
    program.operationalStatus = OPER_PASS;
    program.stage = 0;
    GPIOs.pout0_liberaCatraca.writeSync(1);
    setTimeout(() => {
        GPIOs.pout0_liberaCatraca.writeSync(0);
    }, timers.allowPass_pout0);
    var warnTimeout = setTimeout(warnUser, timers.warnUser);
    var passTimeout = setTimeout(cancelPass, timers.cancelPass);
    GPIOs.nin1_catracaGirou.watch((err, val) => {
        if (!val) {
            clearTimeout(warnTimeout);
            clearTimeout(passTimeout);
            cancelPass();
            wolpacDigitalCounter.setEntry(++entries);
        }
    });
}

function warnUser() {
    var warning = setInterval(() => {
        GPIOs.beepT(800);
        if (program.operationalStatus != OPER_PASS) clearInterval(warning);
    }, 1600);
}

function cancelPass() {
    GPIOs.nin1_catracaGirou.unwatchAll();
    checkNextStatus();
}

function checkNextStatus() {
    if (GPIOs.nin0_barcaCheia.readSync() == 0) {
        program.operationalStatus = OPER_CLOSED;
        fromClosed = true;
//    } else if (GPIOs.uart1cts_checaCarona.readSync()) {
//        program.operationalStatus = OPER_STOP;
    } else {
        program.operationalStatus = OPER_OPEN;
    }

}

module.exports = {
    program
};

const { commands, enableClient, disableClient, getStatus, client } = require("./commands");

client.on('message', (message, remote) => {
    parseProtocol(message, remote);
});

main();
