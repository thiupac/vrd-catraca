/* Copyright (C) Planeta Informática - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * 
 * @file display_rcd
 * @brief Contains all commands to RCD display
 * @author Planeta Engineering <engenharia@planeta.inf.br>
 * @date 2019
 *
 */
"use strict";
/* global require, process, setTimeout, module */
const logger = require('./logger');
const PI = require('./@planetainformatica/pijs');
const MAXLENGTH = 22;
var reader;
var queue = [];

var greenS = 0;
var redS = 0;

var rcdErrors = 0;
const apdu = {
    ATR: [0xFF, 0xCA, 0xE1, 0x00, 0x00],
    displayControl: (text, line, collumn, fontType) => {
        let apdu = [0xAF, 0x02];
        collumn = collumn || 0;
        apdu.push(collumn); // P1
        apdu.push((fontType << 5) | (line & 15)); //P2
        let size = text.length;
        apdu.push(size); // length
        let hexText = text.split('').map(value => value.charCodeAt(0));
        apdu = apdu.concat(hexText);
        return apdu;
    },
    displayClear: [0xAF, 2, 0, 1, 0],
    ledsOn: (green, red) => [0xFE, 0xDF, (green ^ 1) << 6 | (red ^ 1) << 5 | (green & 1) << 3 | (red & 1) << 2, 0x00, 0x00],
    ledsOff: [0xFE, 0xDF, 0x60, 0x00, 0x00]
};

async function setLeds(green, red) {
    greenS = green;
    redS = red;

}

async function initDisplay(startWorker = true) {
    let r = await PI.ListReaders();
    if (r.length == 0) {
        logger.info("Error, cannot find RCD display");
        process.abort();
    }
    reader = r[0];
    resetDisplay();
    if (startWorker)
        worker();
}

function sendDisplay(lineNumber, textMessage) {
    queue.push({
        line: lineNumber,
        text: textMessage
    });
}

async function worker() {
    let transmitResponse = {
        display: null,
        leds: null
    };
    if (queue.length != 0 && reader) {
        var obj = queue.shift();
        if (obj.line == -1) {
            transmitResponse.display = await reader.transmit(apdu.displayClear);
        } else {
            transmitResponse.display = await sendDisplayhw(obj.line, obj.text);
        }
    }
    transmitResponse.leds = await reader.transmit(apdu.ledsOn(greenS, redS));
    if (transmitResponse.display == null && transmitResponse.leds == null) {
        rcdErrors++;
        logger.info("DISPLAY ERROR! transmitResponse", transmitResponse);
        if (rcdErrors == 3) {
            logger.info("Init display again");
            initDisplay(false);
            rcdErrors = 0;
        }
    } else {
        rcdErrors = 0;
    }

    setTimeout(worker, 50);
}

async function sendDisplayhw(lineNumber, textMessage) {
    let ret = 0;
if(!textMessage) textMessage="";
    textMessage = textMessage.substr(0, MAXLENGTH);
    var spaces = parseInt((MAXLENGTH - textMessage.length) / 2);
    var empty = new Array(spaces + 1).join(" ");
    textMessage = empty + textMessage;
    textMessage = textMessage.padEnd(MAXLENGTH);
    if (typeof reader == "object") {
        ret = await reader.transmit(apdu.displayControl(textMessage, lineNumber - 1, 0, 0b001));
    }
    return ret;
}

function resetDisplay() {
    sendDisplay(-1, "");
}

function sendStatus(status) {}

async function resetRCD() {
    logger.info("RESETTING RCD");
    await reader.transmit([0xFF, 0xFE, 0xFC, 0x00, 0x00]);
}

module.exports = {
    sendDisplay,
    resetDisplay,
    sendStatus,
    initDisplay,
    setLeds,
    resetRCD
};
