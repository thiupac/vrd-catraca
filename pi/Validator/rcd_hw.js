"use strict";
/* global require, process, setInterval, setTimeout, clearTimeout, clearInterval, module */
const { Gpio } = require('onoff');
const nin0_barcaCheia = new Gpio(21, 'in', 'both'); //Confirma barca cheia, desativando a leitora
const nin1_catracaGirou = new Gpio(20, 'in', 'both'); //pulso confirmando o giro da catraca
const pout0_liberaCatraca = new Gpio(17, 'out'); //pulso que libera a catraca para passagem
//const pout1_pictograma = new Gpio(18, 'out'); //ativa/desativa o pictograma
const uart1cts_checaCarona = new Gpio(201, 'in'); //Verifica se há carona na passagem
const backlight = new Gpio(140, 'out'); //luz de fundo do visor
const buzzer = new Gpio(363, 'out'); //buzzer...
//const uart1cts_checaCarona = new Gpio(233, 'in', 'both'); //Verifica se há carona na passagem, desativando a leitora em caso
//const tx232_confirmaGiro = new Gpio(198, 'out'); //pulso que libera o giro da catraca

function beepT(time) {
    buzzer.writeSync(1);
    setTimeout(() => buzzer.writeSync(0), time || 500);
}

module.exports = {
    nin1_catracaGirou,
    nin0_barcaCheia,
    pout0_liberaCatraca,
    uart1cts_checaCarona,
    backlight,
    buzzer,
    beepT
};