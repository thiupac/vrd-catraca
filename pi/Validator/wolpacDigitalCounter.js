"use strict";
/* global require, console, Buffer, module, setTimeout */

const SerialPort = require('serialport');
const STX = 0x02;
const ETX = 0x03;
const DLE = 0x10;

const CMD_RESET = 0x00;
const CMD_SET_COUNTERS = 0x01;
const CMD_GET_COUNTERS = 0x02;

const resetCmd = Buffer.concat([
    Buffer.from([DLE, STX]),
    Buffer.from([CMD_RESET]),
    Buffer.from([DLE, ETX])
]);

const getCounterCmd = Buffer.concat([
    Buffer.from([DLE, STX]),
    Buffer.from([CMD_GET_COUNTERS]),
    Buffer.from([DLE, ETX])
]);

let lastErrorObj = {
    status: 0,
    errorMsg: ''
};

let devicePort = null;

function getPort() {
    return devicePort;
}

function getLastTxError() {
    return lastErrorObj;
}

function open(device) {
    /* 
        The baudRate, dataBits, parity and stopBits configuration
        is defined in the Wolpac manual 
    */
    devicePort = new SerialPort(device, {
        baudRate: 9600,
        databits: 8,
        parity: 'none',
        stopBits: 1
    });

    return devicePort;
}

function reset() {
    txBuffer(resetCmd);
}

function setEntry(value) {
    txBuffer(prepareDisplayCounterCmd(value));
}

function getEntry() {
    txBuffer(getCounterCmd);
}

function txBuffer(buffer) {
    //console.info("Sending...", buffer);
    lastErrorObj.status = 0;
    lastErrorObj.errorMsg = '';
    devicePort.write(buffer, function(err) {
        if (err) {
            lastErrorObj.status = 1;
            lastErrorObj.errorMsg = err.message;
            //return console.log('Error on write: ', err.message);
        }
        //console.log('buffer written');
    });
}

function prepareDisplayCounterCmd(entry, exit = 0) {
    let str = "" + entry;
    let pad = "000000";

    /*
        Support only entry counter
    */
    let counterArray = (pad.substring(str.length) + str + pad).split("");
    let buffer = Buffer.concat([
        Buffer.from([DLE, STX]),
        Buffer.from([CMD_SET_COUNTERS]),
        Buffer.from(counterArray),
        Buffer.from([DLE, ETX])
    ]);

    return buffer;
}

function getCmdType(data) {
    return data[2];
}

module.exports = {
    CMD_RESET,
    CMD_GET_COUNTERS,
    CMD_SET_COUNTERS,
    getCmdType,
    getEntry,
    getLastTxError,
    getPort,
    open,
    reset,
    setEntry
};