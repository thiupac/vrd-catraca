/* Copyright (C) Planeta Informática - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * 
 * @file display_electron.js
 * @brief Contains all commands to Electron display
 * @author Planeta Engineering <engenharia@planeta.inf.br>
 * @date 2019
 *
 */

const { app, BrowserWindow } = require('electron');

async function initDisplay() {
    app.on('ready', () => {

        mainWindows = new BrowserWindow
            ({
                webPreferences: {
                    nodeIntegration: true
                },
                width: 1280,
                height: 720
            });

        mainWindows.loadURL(`file://${__dirname}/app/index.html`);
        mainWindows.setMenuBarVisibility(false);
        mainWindows.webContents.on('dom-ready', () => {
            screen_stage = 1;
        })

    });

    app.on('window-all-closed', () => {
        app.quit();
    });
}

async function sendDisplay(lineNumber, textMessage) {
    mainWindows.webContents.send("display", { line: lineNumber, text: textMessage })
}

async function resetDisplay() {
    sendDisplay(1, "")
    sendDisplay(2, "")
    sendDisplay(3, "")
    sendDisplay(4, "")
}

async function sendStatus(status) {
    mainWindows.webContents.send("status", status)
}

module.exports = {
    sendDisplay,
    resetDisplay,
    sendStatus,
    initDisplay
}