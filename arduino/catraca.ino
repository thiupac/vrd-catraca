// C++ code


//output
#define DIR 12
#define ENN 10
#define STEP 11
#define GIROU 8
#define PULSE 267

//input
#define GIR0 6
#define GIR1 7
#define CREDITO 9

//system
#define SERIAL 9600
#define SIZEGIR 4

//signals
#define REST 0
#define PASS 1
#define END 2

//======================================================================
/*
     Definir com true/false o modo de teste
     o modo de teste apresenta entradas pré definidas e roda apenas uma vez
*/
#define TESTE false
#define DEBUG_MODE false
//======================================================================

extern volatile unsigned long timer0_overflow_count;
unsigned long ticks() { return timer0_overflow_count; }

int lastDir = 0;
int credito = HIGH;
int pass = 0;
int signals = 0;
int lastSinal = 0;
int lastDirection = -1;
unsigned long creditTick = 0;

#ifndef TESTE
  #define TESTE false
#endif
#ifndef DEBUG_MODE
  #define DEBUG_MODE false
#endif

#if TESTE
  int count = 0;
#endif

void printInput(int giro0, int giro1, int credito){
#if DEBUG_MODE
  Serial.print("INPUT : [");
  Serial.print("  GIRO0 : ");
  Serial.print(giro0);
  Serial.print("  GIRO1 : ");
  Serial.print(giro1);
  Serial.print("  CREDITO : ");
  Serial.print(!credito);
  Serial.println("]");
#endif
}

void printOutput(int dir, int enn, int step){
#if DEBUG_MODE
  Serial.print("OUTPUT : [");
  Serial.print("  DIR : ");
  Serial.print(dir);
  Serial.print("  ENN : ");
  Serial.print(enn);
  Serial.print("  STEP : ");
  Serial.print(step);
  Serial.println("]");  
  Serial.println("");
#endif
}


void sendProtocol(int dir, int enn, int step){
 printOutput(dir, enn, step);
  digitalWrite(DIR, dir);
  digitalWrite(ENN, enn);
  if(step){
    pulse(step == 41);  
  }
  lastDir = dir;
}

void pulse(bool automatic){
  //int stepDelay = 1875/2;
  int stepDelay = 1875;
  int indexGiro = 0;
  int giroCompleto = 0;
  int gir0 = -1;
  int gir1 = -1;
  
#if TESTE

   const String testGiro[] = { 
      "011",
      "111",
      "101",
      "010",
      "110",
      "100",
      "000"
    };

    const String testGiroBlock[] = {
      "011",
      "011",
      "011",
      "001",
    };

    const String testGiroBlock1[] = {
      "101",
      "101",
      "101",
      "001",
    };

    const String testGiroBlock2[] = {
      "111",
      "111",
      "111",
      "001",
    };

  for(int y = 0; y < 7; y++){ 

    gir0 = (count == 0 ? testGiroBlock[y][0] : count == 1 ? testGiroBlock1[y][0] : count == 2 ? testGiroBlock2[y][0] : testGiro[y][0]) - '0';
    gir1 = (count == 0 ? testGiroBlock[y][1] : count == 1 ? testGiroBlock1[y][1] : count == 2 ? testGiroBlock2[y][1] : testGiro[y][1]) - '0';
    credito = (count == 0 ? testGiroBlock[y][2] : count == 1 ? testGiroBlock1[y][2] : count == 2 ? testGiroBlock2[y][2] : testGiro[y][2]) - '0';
    Serial.print("TEST GIRO #");
    Serial.println(y); 
     
#else
 
 for(;automatic && digitalRead(GIR0) == LOW&& digitalRead(GIR0) == LOW;){
  
     digitalWrite(STEP, HIGH);
     delayMicroseconds(stepDelay);
     digitalWrite(STEP, LOW);
     delayMicroseconds(stepDelay);
     
 }
  while (true) {
    
    gir0 = digitalRead(GIR0);
    gir1 = digitalRead(GIR1);
    //credito = digitalRead(CREDITO);
    
#endif
     printInput(gir0,gir1,credito);
      
     digitalWrite(STEP, HIGH);
     delayMicroseconds(stepDelay);
     digitalWrite(STEP, LOW);
     delayMicroseconds(stepDelay);
      
      
     if(automatic){
       if(gir0 == LOW && gir1 == LOW){
          girou(); 
          break;
       }
       continue;
     }
     
     if(gir0 == LOW && gir1 == HIGH){
         lastDirection = HIGH;
     } else if(gir1 == LOW && gir0 == HIGH){
         lastDirection = LOW;
     }
     
     if(gir0 == LOW && gir1 == HIGH && giroCompleto==0 && credito == LOW){
       giroCompleto++; 
     } else if(gir0 == HIGH && gir1 == HIGH){
      if(giroCompleto==1 && credito == LOW){
        giroCompleto++;  
      } else if (giroCompleto == 0){
        sendProtocol(lastDirection,LOW,PULSE);
#if DEBUG_MODE
          Serial.print(giroCompleto);
          Serial.print(" ");
          Serial.print(lastDirection);
          Serial.print(" ");
          Serial.println(credito);
#endif
        break;
      }
     } else if(gir0 == HIGH && gir1 == LOW && giroCompleto==2 && credito == LOW){
        giroCompleto++;    
     } else if (gir0 == LOW && gir1 == LOW){
       if(credito == LOW && giroCompleto == 3 && signals == PASS){
          girou();
     }
         
#if TESTE
      count++;
      Serial.println("BREAK\n");
#endif
       break;
    }
   } 
}

void girou(){
  credito = HIGH;
  digitalWrite(GIROU, HIGH);
  for(int x = 0; x < 500; x++){
    if(digitalRead(CREDITO) == LOW) break;
  }
  digitalWrite(GIROU, LOW);
  creditTick = 0;
  signals = END;
#if TESTE
  Serial.println("\n\nGIROU\n\n");
#endif
}

void teste(){
    
  String test[] = {
    "100",
    "101",
    "110",
    "111",
    "000",
    "001",
    "010",
    "011"
  };

  Serial.println("Iniciando teste..");
  for(int x = 0; x < 8; x++){
    credito = test[x][0] - '0'; 
    int giro0 = test[x][1] - '0';
    int giro1 = test[x][2] - '0';
    
    process(giro0, giro1, credito);
  }
  
  Serial.println("Finalizando teste..");
  
  lastDir = 0;
  credito = 0;
  pass = 0;
  lastSinal = 0;
}

void process(int giro0, int giro1, int credit){
  
  printInput(giro0,giro1,credito);
  if(credit == LOW && credito == HIGH){
    credito = LOW;
  }
  
  if(credito == LOW && signals == REST) {
#if DEBUG_MODE
      Serial.println("Enable pass");
#endif
     creditTick = ticks();
     signals = PASS;
  } else if(credito == HIGH && signals == END){
    signals = REST;
#if DEBUG_MODE
      Serial.println("Reset pass");
#endif
  }
  if(giro0 == LOW && giro1 == LOW){
    if(creditTick && credito == LOW && ((ticks() - creditTick) >= 4000)){
   // Serial.print("TIMEOUT  ");
   // Serial.println(creditTick);
    sendProtocol(LOW,LOW,41);
    creditTick = 0;
    lastDir = 0;
    pass = 0;
    lastSinal = 0;
    return;
  } else{
    sendProtocol(LOW,HIGH,LOW);
  }
    
    
  } else if(giro0 == HIGH && giro1 == LOW){
    sendProtocol(LOW,LOW,PULSE);
  } else if(giro1 == HIGH && giro0 == LOW){
    sendProtocol(signals == PASS && credito == LOW ? credito : HIGH,LOW,PULSE);
  } else if( giro0 == HIGH && giro1 == HIGH){
    sendProtocol(lastDir,LOW,PULSE);
  }
}

void setup()
{
  Serial.begin(SERIAL);
  
  pinMode(DIR,     OUTPUT);
  pinMode(ENN,     OUTPUT);
  pinMode(STEP,    OUTPUT);
  pinMode(GIROU,   OUTPUT);
  
  pinMode(CREDITO, INPUT);
  pinMode(GIR0,    INPUT);
  pinMode(GIR1,    INPUT);

#if TESTE
  digitalWrite(CREDITO, HIGH);
  teste();
#endif
}

void loop()
{

#if TESTE
  return;
#endif

  int giro0 = digitalRead(GIR0);
  int giro1 = digitalRead(GIR1);
  int credit = credito == LOW ?credito : digitalRead(CREDITO);
  
  process(giro0, giro1, credit);
  delay(100);
}
